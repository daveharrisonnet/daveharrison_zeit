###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false
page "/404.html", relative_links: false

activate :relative_assets do |assets|
  assets.rewrite_ignore = [/404/, /500/]
end

# With alternative layout
# page "/path/to/file.html", layout: :otherlayout

# Proxy pages (http://middlemanapp.com/basics/dynamic-pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", locals: {
#  which_fake_page: "Rendering a fake page with a local variable" }

# General configuration



activate :sprockets
activate :directory_indexes
activate :pagination

#set :url_root, 'https://www.daveharrison.net'

ignore "/templates/*"

# Enable cache buster

#activate :dragonfly_thumbnailer
#page "/sitemap.xml", :layout => false





# -------------------------------------
#   Development Configuration
# -------------------------------------

 configure :development do

   set :css_dir, 'css'
   set :sass_dir, 'css'
   set :js_dir, 'js'
   set :images_dir, 'img'
   set :fonts_dir, 'fonts'
   set :relative_links, true
   set :site_url, ""
end


# -------------------------------------
#   Production Configuration
# -------------------------------------

 configure :production do

  set :css_dir, 'css'
  set :sass_dir, 'css'
  set :js_dir, 'js'
  set :images_dir, 'img'
  set :fonts_dir, 'fonts'
  set :relative_links, true
  set :site_url, ""
 end



# Reload the browser automatically whenever files change
configure :development do
  activate :livereload
end

###
# Helpers
###

# Methods defined in the helpers block are available in templates
#helpers do
#   def some_helper
#     "Helping"
#   end
#end






# config.rb
#case ENV['TARGET'].to_s.downcase
#when 'production'
#  activate :deploy do |deploy|
#    deploy.deploy_method   = :rsync
#    deploy.user  = '' # no default
#    deploy.host            = ''
#    deploy.path            = ''
#    deploy.build_before = true # default: false
    # Optional Settings
   #  deploy.port  = 22 # ssh port, default: 22
    #deploy.clean = true # remove orphaned files on remote host, default: false
#  end
#else
#  activate :deploy do |deploy|
#    deploy.deploy_method   = :rsync
#    deploy.user  = '' # no default
#    deploy.host            = ''
#    deploy.path            = ''
#    deploy.build_before = true # default: false
    # Optional Settings

    #deploy.port  = 22 # ssh port, default: 22
    #deploy.clean = true # remove orphaned files on remote host, default: false
#ßend

helpers do
  def markdown(text)
    renderer = Redcarpet::Render::HTML.new
    Redcarpet::Markdown.new(renderer).render(text)
  end

  def image_or_missing(image)
    if image
      yield image
    else
      image_tag "/img/missing-image.png"
    end
  end










end
